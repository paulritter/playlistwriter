package main

import (
	"io/ioutil"
	"os"
	"log"
	"strings"
	"os/user"
	"path/filepath"
	"flag"
	"github.com/dhowden/tag"
)

type Track struct {
	Title string
	Artist string
	Playlists []string
	Filename string
}

var settings map[string] string

func init() {
	JobQueue = make(chan Job)
	settings = make(map[string] string)
	var ARGfolderletter string
	var ARGprefix string
	var ARGmusicdir string
	var ARGexport string

	tmp, _ := user.Current()

	flag.StringVar(&ARGfolderletter, "dirs", "false", "true, if tracks are in separate folders starting with the first letter.")
    flag.StringVar(&ARGprefix, "prefix", "", "A path to prefix for all tracks, e. g. /Musik/Tracks/")
	flag.StringVar(&ARGmusicdir, "musicdir", ".", "The directory to search")
	flag.StringVar(&ARGexport, "outdir", tmp.HomeDir, "The directory to output M3U-files")
    flag.Parse()


	settings["USEFOLDERLETTER"] = ARGfolderletter
	settings["PREFIX"] = ARGprefix
	settings["MUSICDIR"] = ARGmusicdir
	settings["EXPORTPATH"] = ARGexport
}

func main() {
	var playlists map[string] *Playlist
	var exists map[string] bool
	
	
	playlists = make(map[string] *Playlist)
	exists = make(map[string] bool)
	
	files, err := ioutil.ReadDir(settings["MUSICDIR"])
	if err != nil {
		log.Fatal(err)
	}
	
	for _, value := range files {
		
		track := getMeta(value.Name())
		
		for _, pl := range track.Playlists {
		if !exists[pl] {
			playlists[pl] = NewPlaylist(pl)
			exists[pl] = true
		}
		playlists[ pl ].addTitle( track )
		}
	}
	
	writePlaylists(playlists)
}


func getMeta(filename string) Track {
	result := Track{}
	if strings.HasSuffix(filename, ".mp3") {
		file, err := os.Open(filename)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
		track, error := tag.ReadFrom(file)
		if error != nil {
			log.Fatal(error)
		}
		result.Title = track.Title()
		result.Artist = track.Artist()
		result.Playlists = strings.Split(track.Comment(), ";")
		result.Filename = filename
		
	} else {
		log.Println(StringJoin(filename, ": No MP3"))
	}
	return result
}

func writePlaylists(playlists map[string] *Playlist ) {
	dispatcher := NewDispatcher(MaxWorker)
	dispatcher.Run()
	for _,pl := range playlists {
		work := Job{Payload: pl, Function: 1 }
		JobQueue <- work
	}
}

func writePlaylist(pl *Playlist) {
	f, err := os.OpenFile(filepath.Join(settings["EXPORTPATH"],StringJoin(pl.Title,".m3u")), os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	if _, err := f.Write([]byte(pl.String())); err != nil {
		f.Close() // ignore error; Write error takes precedence
		log.Fatal(err)
	}
}
