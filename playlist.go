package main

type Playlist struct {
	Title string
	content string
}

func NewPlaylist(title string) *Playlist {
	pl := new(Playlist)
	pl.Title = title
	pl.content = "#EXTM3U\n"
	return pl
}

func (pl *Playlist) String() string {
	return pl.content
}

func (pl *Playlist) addTitle(track Track) {
		pl.content = StringJoin(pl.content, "#EXTINF:-1,", track.Artist," - ",track.Title, "\n")
		if "true" == settings["USEFOLDERLETTER"] {
			pl.content = StringJoin(pl.content, settings["PREFIX"], getFirstLetter(track.Filename), "/", track.Filename, "\n")
		} else {
			pl.content = StringJoin(pl.content, settings["PREFIX"], track.Filename, "\n")
		}
}