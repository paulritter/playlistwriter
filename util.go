package main

import (
	"strings"
)

func removeSuffix(title string) string {
	return title[len(title)-len(".mp3"):]
}

func getFirstLetter(title string) string {
	return title[:1]
}

func StringJoin(strs ...string) string {
	var sb strings.Builder
	for _, str := range strs {
		sb.WriteString(str)
	}
	return sb.String()
}